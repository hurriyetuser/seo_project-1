# -*- coding: UTF-8 -*-
# @author: Ilkay Tevfik Devran
# @updatedDate = 04.03.2019
# @version: 1.0

import os
import json
from datetime import datetime
from localMSSQL.localDBMSSQL import local_Dbconnection as DB
import codecs

def createLog(data):
    logFilePath = os.path.join(os.path.dirname( os.path.realpath( __file__ )) , 'last5Mins.json')

    with codecs.open(logFilePath, 'w+', encoding='utf-8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)
    outfile.close()

def prepare_5_mins_data():
    db = DB()
    data = {'web':{}, 'mobile':{}}

    # Get active search terms
    search_terms = db.get_active_words()

    if len(search_terms) == 0:
        return data # return empty data dic
    else:
        for term in search_terms:
            
            keyword = term[1].encode('utf-8').decode('utf-8').strip()
            results = db.get_last_5_mins(keyword.encode('utf-8')) 
            
            data['web'][keyword]=[]
            data['mobile'][keyword]=[]
            
            #print(keyword.encode('utf-8'))
            if results == None:
                print("Results NONE")
            else:
                for row in results:
                    platform, cite, rank, _ = row
                    platform, cite = platform.encode('utf-8').decode('utf-8').strip(), cite.encode('utf-8').decode('utf-8').strip()
                    tmp = {"company": cite.encode('utf-8').strip(), "rank": rank, "details":[]}
                    
                    if platform == 'mobile':
                        data['mobile'][keyword].append(tmp)
                    else:
                        data['web'][keyword].append(tmp)

                    detailsForcurrentKeyword = db.get_comapny_details_for_currentday(keyword.encode('utf-8'), cite.encode('utf-8'), platform.encode('utf-8'))
                    for detail in  detailsForcurrentKeyword:
                        _,_,_,_,totalView = detail
                        data[platform][keyword][-1]['details'].append(totalView)
    
    createLog(data)
    print("Last 5 mins json file has been prepared.\n\n")