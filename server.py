#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# @author: Ilkay Tevfik Devran
# @updatedDate: 15.03.2019
# @version: 2.0.1 
#

from flask import Flask, request, make_response, Response, jsonify
import platform
import json
from datetime import datetime
import codecs
from localMSSQL.localDBMSSQL import local_Dbconnection as DB

from crawler import main as RUN_CRAWLER


# local database connection
db = DB()


app = Flask(__name__)


@app.route('/')
def index():
    return "Hello World!"

@app.route('/api/tasks/date-interval', methods=['GET'])
def get_day_and_date_interval_result():
    #/api/tasks/date-interval?beginDate=2019-03-14&endDate=2019-03-15
    data = {'web':{}, 'mobile':{}}
    try:
        beginDate = request.args.get('beginDate', default = datetime.now().strftime("%Y-%m-%d"), type = str)
        endDate = request.args.get('endDate', default = datetime.now().strftime("%Y-%m-%d"), type = str)

        cites = db.get_distinct_cites() # Get cites in db
        activeWords = db.get_active_words() # Get active search terms

        if len(activeWords) == 0:
            return data # return empty data dic
        else:
            for platform in ['web','mobile']: # platforms
                for term in activeWords:    # active words
                    keyword = term[1].encode('utf-8').decode('utf-8').strip()
                    data[platform][keyword]=[]

                    for cite in cites:  # cites
                        #print(cite[0])
                        cite = cite[0].strip().encode('utf-8').decode('utf-8')
                        results = db.get_date_interval_company_details(keyword.encode('utf-8'), cite.encode('utf-8'), platform, beginDate, endDate)
                        tmp = {"company": cite, "details":[]}
                        for row in results: # query results
                            _, _, _, _, totalView = row
                            tmp['details'].append(totalView)
                        data[platform][keyword].append(tmp)
                            
        return json.dumps(data, indent=4, ensure_ascii = False),200
    
    except Exception as e:
        print ("[get_day_and_date_interval_result]\n\n" + e)
        return json.dumps(data, indent=4, ensure_ascii = False), 200


@app.route('/api/tasks/time-interval', methods=['GET'])
def get_day_and_time_interval_result():
    #/api/tasks/time-interval?date=2019-03-01&beginHour=1&endHour=2
    data = {'web':{}, 'mobile':{}}

    try:
        paramDate = request.args.get('date', default = datetime.now().strftime("%Y-%m-%d"), type = str)
        beginHour = request.args.get('beginHour', default = 1, type = int)
        endHour = request.args.get('endHour', default = 2, type = int)

        

        cites = db.get_distinct_cites() # Get cites in db
        activeWords = db.get_active_words() # Get active search terms

        if len(activeWords) == 0:
            return data # return empty data dic
        else:
            for platform in ['web','mobile']: # platforms
                for term in activeWords:    # active words
                    keyword = term[1].encode('utf-8').decode('utf-8').strip()
                    data[platform][keyword]=[]

                    for cite in cites:  # cites
                        #print(cite[0])
                        cite = cite[0].strip().encode('utf-8').decode('utf-8')   
                        results = db.get_company_details_with_dateParameter(keyword.encode('utf-8'), cite.encode('utf-8'), platform, paramDate, beginHour, endHour)
                        tmp = {"company": cite, "details":[]}
                        for row in results: # query results
                            _, _, _, _, totalView = row
                            tmp['details'].append(totalView)
                        data[platform][keyword].append(tmp)
                            
        return json.dumps(data, indent=4, ensure_ascii = False),200
    
    except Exception as e:
        print ("[get_day_and_time_interval_result]\n\n" + e)
        return json.dumps(data, indent=4, ensure_ascii = False), 200


@app.route('/api/tasks/last-five-mins', methods=['GET'])
def get_last_five_mins():
    data = {'web':{}, 'mobile':{}}
    try:
        logFilePath = './last5Mins.json'
        with codecs.open(logFilePath, encoding=('utf-8')) as f:
            data = json.load(f)
        f.close()
        return json.dumps(data, indent=4, ensure_ascii = False), 200
    except Exception as e:
        print ("[get_last_five_mins]\n\n" + e)
        return json.dumps(data, indent=4, ensure_ascii = False), 200


@app.route('/api/tasks/set-active-words', methods=['POST'])
def activateSeacrhWords():
    try:
        response_json = request.get_json()
        
        # TODO this company parameter is going to be edited in future (Koray Hoca)
        company = 'Hurriyet' 

        # Reset activeness of words
        db.reset_activeness_of_words()
        # Insert words into db and update activeness of them
        for word in response_json['search_words']:
            #print (word)
            db.insert_into_Words_table(word, company, 1)

        return json.dumps(response_json, indent=4), 200
    
    except Exception as e:
        print ("[activateSeacrhWords]\n\n" + e)


@app.route('/api/tasks/run-crawler', methods=['GET'])
def run_crawler():
    try:
        RUN_CRAWLER()
        return "Crawler has been run properly."
    except:
        return "Crawler couldn't run properly."


if __name__ == '__main__':
    app.run(debug=True, port=3000)


# curl -i -H "Content-Type: application/json" -X POST -d '{"search_words":["sevgililer günü","meme kanseri","milli takım","yerel seçim"]}' http://localhost:3000/api/tasks

