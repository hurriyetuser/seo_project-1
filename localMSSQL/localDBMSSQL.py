#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# @author: Ilkay Tevfik Devran
# @updatedDate: 15.03.2019
# @version: 2.0
#


import pymssql # mssql
import configparser
import os
      
class local_Dbconnection():
    def __init__(self):
        self.connect_to_local_db()
    
    def connect_to_local_db(self):
        # Config File Parsing #
        config = configparser.ConfigParser()
        currentDir = os.path.dirname(os.path.abspath(__file__))
        config.read(os.path.join(currentDir, 'config.cfg'))
        
        self.cnxn = pymssql.connect(
            server=config.get('mssql', 'host'),
            user=config.get('mssql', 'user'),
            password=config.get('mssql', 'password'),
            database=config.get('mssql', 'database'),
            host=config.get('mssql', 'host'))
        self.cursor = self.cnxn.cursor()

    def get_active_words(self):
        query = """SELECT word_id, Keyword FROM [Uluc].[dbo].[Words] WHERE IsActive = 1"""
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL run_select_query() ERROR]" + e.__str__()

    def insert_search_results_to_db(self, platform, cite, rank, publishTime, title, url, wordID, searchOnTime):
        query = """INSERT INTO [Uluc].[dbo].[CarouselResults] (Platform, Cite, Rank, PublishTime, Title, Url, InsertedDate, wordID, SearchOnTime) \
                    VALUES ('%s', '%s', %s, '%s', '%s', '%s', GETDATE(), %s, '%s');""" % (platform.encode('utf-8'), cite.encode('utf-8'), 
                    rank, publishTime.encode('utf-8'), title.encode('utf-8').replace("'","''"), url.encode('utf-8'), wordID, searchOnTime)
        
        try:
            self.cursor.execute(query)
            self.cnxn.commit()
        except Exception as e:
            print(" ".join(['[ERROR] When inserting:\n',query,"\n\n", e.__str__()]))
    
    def insert_into_Words_table(self, keyword, company, isactive):
        """ NOTE: For GETDATE, company parameters is just 'Hurriyet'

        """
        
        # Check if a word is already exist in db
        def isInDB(keyword, company):
            query = """ SELECT word_id, Keyword FROM [Uluc].[dbo].[Words] \
                WHERE Keyword = '%s' AND Company = '%s' """ % (keyword, company)
            
            try:
                self.cursor.execute(query)
                result = self.cursor.fetchone()
            except Exception as e:
                print "[MSSQL run_select_query() ERROR]" + e.__str__()

            if result != None:
                return True

            return False

        
        # GET QUERY TO BE RUN that is update or insert
        toBeUpdated = False # for print conseole status report
        if isInDB(keyword, company) == False:
            query = """INSERT INTO [Uluc].[dbo].[Words] (Keyword, Company, IsActive, CreatedDate, LastModifiedDate) \
                VALUES ('%s','%s', %s, GETDATE(), GETDATE());""" % (keyword, company, isactive)
            print("New word is going to be inserted. -> ", keyword)   
        else:
            query = """ UPDATE [Uluc].[dbo].[Words] SET IsActive = %s, LastModifiedDate = GETDATE() WHERE Keyword = '%s'; """ % (isactive, keyword)
            print("Update is going to be performed on -> ", keyword)
            toBeUpdated = True

        # Run determined query
        try:
            self.cursor.execute(query)
            self.cnxn.commit()
        except Exception as e:
            if toBeUpdated:
                print(" ".join(['[ERROR] When Updating in WORDS table:\n', query]))
            else:
                print(" ".join(['[ERROR] When Inserting into WORDS table:\n', query]))

    def reset_activeness_of_words(self):
        # Reset isActive of each word in Words table
        query = """ UPDATE [Uluc].[dbo].[Words] SET `IsActive` = 0, `LastModifiedDate` = GETDATE(); """
        
        try:
            self.cursor.execute(query)
            self.cnxn.commit()
            print("RESETTING OK!")
        except Exception as e:
            print(" ".join(['[ERROR] When resetting isActive of  words in Words table:\n', query]))
    
    def get_last_5_mins(self, keyword):
        query = """EXEC [dbo].[sp_Last_Five_Mins_Order] @Keyword = '%s' """ % (keyword)
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL get_last_5_mins() ERROR]" + e.__str__()

    def get_comapny_details_for_currentday(self, keyword, cite, platform):
        query = """EXEC [dbo].[sp_CurrentDay_Company_Order] @Keyword = '%s', @Cite = '%s', @Platform = '%s'""" % (keyword, cite, platform)
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL get_comapny_details_for_currentday() ERROR]" + e.__str__()

    def get_company_details_with_dateParameter(self, keyword, cite, platform, paramDate, beginHour, endHour):
        query = """EXEC [dbo].[sp_Companies_Order_With_DateParameter] 
            @Keyword = '%s', @Cite = '%s', @Platform = '%s', @ParamDate = '%s', 
            @BeginHour = %s,  @EndHour = %s""" % (keyword, cite, platform, paramDate, beginHour, endHour)
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL get_company_details_with_dateParameter() ERROR]" + e.__str__()

    def get_distinct_cites(self):
        query = """SELECT DISTINCT Cite FROM [Uluc].[dbo].[CarouselResults]"""
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL get_distinct_cites() ERROR]" + e.__str__()
        
    def get_date_interval_company_details(self, keyword, cite, platform, beginParamDate, endParamDate):
        query = """EXEC [dbo].[sp_Companies_Order_DateParam] 
            @Keyword = '%s', @Cite = '%s', @Platform = '%s',
            @BeginParamDate = '%s',  @EndParamDate = '%s'""" % (keyword, cite, platform, beginParamDate, endParamDate)
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except Exception as e:
            print "[MSSQL get_day_interval_company_details() ERROR]" + e.__str__()



"""
-- Call strore procedure for detail of company
EXEC [dbo].[sp_CurrentDay_Company_Order] @Keyword = 'son dakika', @Cite = 'Hurriyet', @Platform = 'web'

-- Call strore procedure for last five mins order to given word
EXEC [dbo].[sp_Last_Five_Mins_Order] @Keyword = 'yerel seçim'

-- Call strore procedure time interval
EXEC [dbo].[sp_Companies_Order_With_DateParameter] @Keyword = 'son dakika', @Cite = 'Hurriyet', @Platform = 'web', @ParamDate = '2019-02-28', @BeginHour = 13,  @EndHour = 14


-- Call strore procedure day interval
EXEC [dbo].[sp_Companies_Order_DateParam]
	@Keyword = 'deprem',
	@Cite = 'hurriyet',
	@Platform = 'web',
	@BeginParamDate = '2019-03-11',
	@EndParamDate = '2019-03-12'
"""
